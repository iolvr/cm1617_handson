package cm.ua.pt.cmlab0handson;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;

public class MainActivity extends AppCompatActivity {

    private ArrayAdapter<String> forecastListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //String[] daysLabels = new String[] {"dia 01", "dia 02", "dia 03"};

        String[] daysLabels =getTheWeatherForecast();

                forecastListAdapter = new ArrayAdapter<String>(
                this, // The current context (this activity)
                R.layout.list_item_meteo, // The name of the layout ID.
                R.id.textDayForecast, // The ID of the textview to populate.
                daysLabels);

        ListView daysList = (ListView) findViewById(R.id.list_of_days);
        daysList.setAdapter( forecastListAdapter);


        getTheWeatherForecast();
    }

    private String [] getTheWeatherForecast() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        WeatherParser parser = new WeatherParser();
        String[] days = new String[0];
        try {
            days =   parser.getWeatherDataFromJson( parser.callOpenWeatherForecast(), 7);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return days;
    }
}
